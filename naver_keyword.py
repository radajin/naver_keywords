
# coding: utf-8

# In[1]:


import requests, json
from bs4 import BeautifulSoup
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


# In[2]:


engine = create_engine("mysql://root:dss@13.125.81.55/world?charset=utf8")
base = declarative_base()


# In[3]:


class NaverKeyword(base):
    __tablename__ = "naver"

    id = Column(Integer, primary_key=True)
    rank = Column(Integer, nullable=False)
    keyword = Column(String(20), nullable=False)
    rdate = Column(TIMESTAMP, nullable=False)

    def __init__(self, rank, keyword):
        self.rank = rank
        self.keyword = keyword

    def __repr__(self):
        return "<NaverKeyword {}, {}>".format(self.rank, self.keyword)


# In[4]:


def crawling():
    response = requests.get("https://www.naver.com/")
    dom = BeautifulSoup(response.content, "html.parser")
    keywords = dom.select(".ah_roll_area > .ah_l > .ah_item")
    results = []
    for keyword in keywords:
        rank = keyword.select_one(".ah_r").text
        keyword = keyword.select_one(".ah_k").text
        results.append( NaverKeyword(rank, keyword) )
    return results


# In[5]:


def save(keywords):

    # make session
    maker = sessionmaker(bind=engine)
    session = maker()

    # save datas
    session.add_all(keywords)
    session.commit()

    # close session
    session.close()


# In[6]:


def send_slack(msg, channel="#dss", username="provision_bot" ):
    webhook_URL = "https://hooks.slack.com/services/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"
    payload = {
        "channel": channel,
        "username": username,
        "icon_emoji": ":provision:",
        "text": msg,
    }
    response = requests.post(
        webhook_URL,
        data = json.dumps(payload),
    )
    return response


# In[7]:


base.metadata.create_all(engine)
keywords = crawling()
save(keywords)


# In[9]:


send_slack("naver crawling done!")
